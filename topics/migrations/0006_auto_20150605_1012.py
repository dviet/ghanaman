# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('topics', '0005_auto_20150605_0947'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='message',
            field=tinymce.models.HTMLField(),
        ),
        migrations.AlterField(
            model_name='rule',
            name='content',
            field=tinymce.models.HTMLField(),
        ),
    ]
