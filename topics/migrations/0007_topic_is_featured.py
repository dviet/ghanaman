# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('topics', '0006_auto_20150605_1012'),
    ]

    operations = [
        migrations.AddField(
            model_name='topic',
            name='is_featured',
            field=models.BooleanField(default=0),
            preserve_default=True,
        ),
    ]
