# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('topics', '0003_topic_is_deleted'),
    ]

    operations = [
        migrations.AddField(
            model_name='topic',
            name='num_views',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
