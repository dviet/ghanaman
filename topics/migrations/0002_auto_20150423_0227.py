# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('topics', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topic',
            name='slug',
            field=models.SlugField(max_length=255, unique=True, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='topiccategory',
            name='slug',
            field=models.SlugField(max_length=255, unique=True, null=True, blank=True),
        ),
    ]
