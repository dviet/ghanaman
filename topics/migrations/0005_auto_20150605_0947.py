# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('topics', '0004_topic_num_views'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='message',
            field=ckeditor.fields.RichTextField(),
        ),
        migrations.AlterField(
            model_name='rule',
            name='content',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
