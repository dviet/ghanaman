# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('topics', '0007_topic_is_featured'),
    ]

    operations = [
        migrations.AddField(
            model_name='topic',
            name='updated_at',
            field=models.DateTimeField(default=datetime.date(2015, 6, 17), auto_now_add=True),
            preserve_default=False,
        ),
    ]
