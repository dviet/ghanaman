from django.contrib import admin

# Register your models here.
from topics.models import Topic, TopicCategory, Comment, Rule


class TopicAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug": ("subject",)}

	list_display = ('author', 'subject', 'date', 'updated_at', )  
admin.site.register(Topic, TopicAdmin)


class TopicCategoryAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug": ("title",)}

	list_display = ('title', 'is_active')  
admin.site.register(TopicCategory, TopicCategoryAdmin)


class CommentAdmin(admin.ModelAdmin):

	list_display = ('author', 'date', 'topic')  
admin.site.register(Comment, CommentAdmin)


class RuleAdmin(admin.ModelAdmin):

	list_display = ('content',)  
admin.site.register(Rule, RuleAdmin)