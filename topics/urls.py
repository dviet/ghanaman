from django.conf.urls import patterns, url

from topics.views import board, newtopic, topic,\
						 newcomment, editcomment, edittopic, deletetopic,\
						 featured_links, featured_link


urlpatterns = patterns('',
	url(r'^board/(?P<slug>[\w-]+)/$', board, name='board'),
	url(r'^newtopic/(?P<board_id>\d+)/$', newtopic, name='newtopic'),
	url(r'^topic/(?P<slug>[\w-]+)/$', topic, name='topic'),
	url(r'^newcomment/(?P<topic_id>\d+)/$', newcomment, name='newcomment'),
	url(r'^comment/edit/(?P<comment_id>\d+)/$', editcomment, name='editcomment'),
	url(r'^edittopic/(?P<topic_id>\d+)/$', edittopic, name='edittopic'),
	url(r'^deletetopic/(?P<topic_id>\d+)/$', deletetopic, name='deletetopic'),
	url(r'^links/featured/$', featured_links, name='featured_links'),
	url(r'^links/featured/(?P<slug>[\w-]+)/$', featured_link, name='featured_link')
)