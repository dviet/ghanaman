from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from tinymce.models import HTMLField
from topics.utils import generate_slug

# Create your models here.

class TopicCategory(models.Model):
	title = models.CharField(max_length=255)
	description = models.TextField(null=True, blank=True)
	is_active = models.BooleanField(default=True)
	slug = models.SlugField(unique=True, null=True, blank=True, max_length=255)
	parent = models.ForeignKey('self', null=True, blank=True, related_name='board_parent')

	def save(self, *args, **kwargs):
		self.slug = generate_slug(self, self.title)

		super(TopicCategory, self).save(*args, **kwargs)

	def __unicode__(self):  
		return self.title

	def get_all_parent(self, include_self=True):
		r = []
		if include_self:
			r.append(self)
		if self.parent:
			r.append(self.parent.get_all_parent(include_self=False))

		if not r:
			r = self
		return r


class Topic(models.Model):
	author = models.ForeignKey(User, related_name='topic_author')
	subject = models.CharField(max_length=255)
	slug = models.SlugField(unique=True, null=True, blank=True, max_length=255)
	date = models.DateTimeField(default=timezone.now)
	updated_at = models.DateTimeField(auto_now_add=True, auto_now=False)
	category = models.ForeignKey(TopicCategory, related_name='topic_category')
	is_deleted = models.BooleanField(default=0)
	is_featured = models.BooleanField(default=0)
	num_views = models.IntegerField(default=0)

	def save(self, *args, **kwargs):
		try:
			if not self.is_update_view:
				self.slug = generate_slug(self, self.subject)
		except:
			self.slug = generate_slug(self, self.subject)

		super(Topic, self).save(*args, **kwargs)

	def __unicode__(self):  
		return self.subject

class Comment(models.Model):
	author = models.ForeignKey(User, related_name='comment_author')
	message = HTMLField()
	date = models.DateTimeField(default=timezone.now)
	topic = models.ForeignKey(Topic, related_name='comment_topic')
	quote = models.ForeignKey('self', null=True, blank=True)
	is_first = models.BooleanField(default=False)

	def __unicode__(self):  
		return self.topic.subject


class CommentFile(models.Model):
	comment = models.ForeignKey(Comment, related_name='comment_file')
	file = models.FileField(upload_to = 'files', blank=True, null=True)
	image = models.ImageField(upload_to = 'images', blank=True, null=True)

	def __unicode__(self):  
		return self.comment


class Rule(models.Model):
	content = HTMLField()