from django.shortcuts import render, render_to_response, Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models import Count, Min, Sum, Avg, Max, Q
from django.utils import timezone
from topics.models import TopicCategory, Topic, Comment, Rule
from index.views import page_not_found
from topics.forms import TopicForm, CommentForm
from userprofile.models import Like, Share, UserProfile
from tracking.models import Visitor, Pageview
from advertises.views import get_random_advertises

# Create your views here.

def board(request, slug):
	# get number of viewer (member and guest)
	viewers = get_viewer(request, request.get_full_path())

	try:
		board = TopicCategory.objects.get(slug=slug)
	except TopicCategory.DoesNotExist:
		return page_not_found(request)

	categories = TopicCategory.objects.filter(parent=board)

	q = request.GET.get('q', 'updated')
	
	if q == "posts":
		topics_list = Topic.objects.filter(Q(category=board) | Q(category__in=categories), is_deleted=0).annotate(num_posts=Count('comment_topic')).order_by('-num_posts', '-subject')
	elif q == "new":
		topics_list = Topic.objects.filter(Q(category=board) | Q(category__in=categories), is_deleted=0).annotate(new=Max('comment_topic__date')).order_by('-new', '-subject')
	elif q == "view":
		topics_list = Topic.objects.filter(Q(category=board) | Q(category__in=categories), is_deleted=0).order_by('-num_views')
	else:
		topics_list = Topic.objects.filter(Q(category=board) | Q(category__in=categories), is_deleted=0).order_by('-date')

	for topic in topics_list:
		topic.last_comment = topic.comment_topic.all().order_by('-id')[0]

	paginator = Paginator(topics_list, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		topics = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		topics = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		topics = paginator.page(paginator.num_pages)

	# list of advertises
	advertises = get_random_advertises()
	
	return render(request, "board.html", {	
											'board': board,
											'topics': topics,
											'categories': categories,
											'viewers_member': viewers[0],
											'viewers_guest': viewers[1],
											'advertises': advertises,
											'mod_permission': check_mod(request.user, board),
											'q': q
											})

def get_viewer(request, path):
	members = []
	guest = 0
	page_views = Pageview.objects.values('url', 'visitor_id')\
							.annotate(ucount=Count("url"), vcount=Count("visitor"))\
							.filter(url__exact=path)\
							.order_by("-ucount")
	if page_views:
		for page_view in page_views:
			# print page_view['visitor_id']
			# get corresponding user through visitor
			try:
				visitor = Visitor.objects.get(session_key__exact=page_view['visitor'],\
											expiry_time__gt=timezone.now(),\
											end_time__isnull=True	
										)

				if visitor.user:
					members.append(visitor.user)
				else:
					guest += 1
			except Exception, e:
				pass

	return [members, guest]

@login_required
def newtopic(request, board_id):
	user = User.objects.get(pk=request.user.id)
	try:
		board = TopicCategory.objects.get(pk=board_id)
	except TopicCategory.DoesNotExist:
		return page_not_found(request)

	parent_boards = TopicCategory.objects.filter(board_parent=board)
	form_topic = TopicForm()
	form = CommentForm()
	if request.method == 'POST':
		form_topic = TopicForm(request.POST)
		form = CommentForm(request.POST)
		if form.is_valid() and form_topic.is_valid():
			topic = form_topic.save(commit=False)
			topic.author = user
			topic.category = board
			topic.save()

			comment = form.save(commit=False)
			comment.author = user
			comment.topic = topic
			comment.is_first = True
			comment.save()

			return HttpResponseRedirect('/board/' + board.slug)

	try:
		rule = Rule.objects.get(pk=1)
	except Rule.DoesNotExist:
		rule = None

	return render(request, "newtopic.html", {	
											'board': board,
											'parent_boards': parent_boards,
											'form': form,
											'form_topic': form_topic,
											'rule': rule,
											})

def topic(request, slug):
	viewers = get_viewer(request, request.get_full_path())
	try:
		topic = Topic.objects.get(slug=slug)
		topic.num_views = topic.num_views + 1
		topic.is_update_view = True
		topic.save()
	except Topic.DoesNotExist:
		return page_not_found(request)

	comments = Comment.objects.filter(topic=topic).order_by('date')
	for comment in comments:
		comment.likes = Like.objects.filter(comment=comment).count()
		comment.shares = Share.objects.filter(comment=comment).count()
		if request.user.is_authenticated():
			comment.is_liked = Like.objects.filter(comment=comment, user=request.user)
			comment.is_shared = Share.objects.filter(comment=comment, user=request.user)
	paginator = Paginator(comments, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		comments = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		comments = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		comments = paginator.page(paginator.num_pages)

	# list of advertises
	advertises = get_random_advertises()

	return render(request, "topic.html", {	
											'topic': topic,
											'comments': comments,
											'viewers_member': viewers[0],
											'viewers_guest': viewers[1],
											'advertises': advertises
											})

@login_required
def newcomment(request, topic_id):
	user = User.objects.get(pk=request.user.id)
	try:
		topic = Topic.objects.get(pk=topic_id)
	except Topic.DoesNotExist:
		return page_not_found(request)

	if request.method == 'POST':
		form = CommentForm(request.POST)
		if form.is_valid():
			comment = form.save(commit=False)
			comment.author = user
			comment.topic = topic
			comment.save()

			return HttpResponseRedirect('/topic/' + topic.slug)

	else:
		form = CommentForm()

	try:
		rule = Rule.objects.get(pk=1)
	except Rule.DoesNotExist:
		rule = None

	return render(request, "newcomment.html", {	
											'form': form,
											'rule': rule,
											})

@login_required
def editcomment(request, comment_id):
	try:
		comment = Comment.objects.get(pk=comment_id)
	except Comment.DoesNotExist:
		return page_not_found(request)

	user = User.objects.get(pk=request.user.id)
	if not user == comment.author:
		return page_not_found(request)

	if request.method == 'POST':
		form = CommentForm(request.POST, instance=comment)
		if form.is_valid():
			form.save()

			return HttpResponseRedirect('/topic/' + comment.topic.slug + '#' + str(comment.id))

	else:
		form = CommentForm(instance=comment)

	try:
		rule = Rule.objects.get(pk=1)
	except Rule.DoesNotExist:
		rule = None

	return render(request, "newcomment.html", {	
											'form': form,
											'rule': rule,
											})

@login_required
def edittopic(request, topic_id):
	try:
		topic = Topic.objects.get(pk=topic_id)
	except Topic.DoesNotExist:
		return page_not_found(request)

	user = User.objects.get(pk=request.user.id)
	
	# check chicken permission
	if not check_mod(user, topic.category):
		if not user == topic.author:
			return page_not_found(request)


	comment = Comment.objects.get(topic=topic, is_first=True)

	if request.method == 'POST':
		form = CommentForm(request.POST, instance=comment)
		form_topic = TopicForm(request.POST, instance=topic)
		if form.is_valid() and form_topic.is_valid():
			form.save()
			topic = form_topic.save()

			return HttpResponseRedirect('/topic/' + topic.slug)

	else:
		form = CommentForm(instance=comment)
		form_topic = TopicForm(instance=topic)

	try:
		rule = Rule.objects.get(pk=1)
	except Rule.DoesNotExist:
		rule = None

	return render(request, "newtopic.html", {	
											'form': form,
											'form_topic': form_topic,
											'rule': rule,
											})


def deletetopic(request, topic_id):

	try:
		topic = Topic.objects.get(id=topic_id)
		# check chicken permission
		if not check_mod(request.user, topic.category):
			return page_not_found(request)

		topic.is_deleted = 1
		topic.save()
		return HttpResponseRedirect('/board/' + topic.category.slug)
	except:
		return page_not_found(request)


def check_mod(user, category):
	try:
		user_profile = UserProfile.objects.get(user_id=user.id)
	except:
		return False

	if (user_profile.mod_type == '1') or (user_profile.mod_type == '2'):
		mod_categories = get_categories_mod(user)
		
		if category.id in mod_categories:
			return True

	return False


def get_categories_mod(user):
	
	list_categories = []
	user_profile = UserProfile.objects.get(user_id=user.id)
	
	for category in user_profile.mod_category.all():
		
		list_categories.append(category.id)
		# append child categories
		if (category.parent_id is None) and (user_profile.mod_type == '1'):
			
			child_categories = TopicCategory.objects.filter(parent_id=category.id)

			for child_category in child_categories:
				# append to list if it isn't belong to list
				if child_category.id not in list_categories:
					list_categories.append(child_category.id)

	return list_categories


def featured_links(request):
	try:
		featured_links = Topic.objects.filter(is_deleted=False, is_featured=True)
	except Exception, e:
		return page_not_found(request)

	paginator = Paginator(featured_links, settings.NUMBER_ITEM_PERPAGE)
	page = request.GET.get('page')
	print 
	try:
		featured_links = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		featured_links = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		featured_links = paginator.page(paginator.num_pages)

	return render(request, 'featured_links.html', {'featured_links': featured_links})

def featured_link(request, slug):
	viewers = get_viewer(request, request.get_full_path())
	try:
		topic = Topic.objects.get(slug=slug, is_deleted=False, is_featured=True)
		topic.num_views = topic.num_views + 1
		topic.is_update_view = True
		topic.save()
	except Topic.DoesNotExist:
		return page_not_found(request)

	comments = Comment.objects.filter(topic=topic).order_by('date')
	for comment in comments:
		comment.likes = Like.objects.filter(comment=comment).count()
		comment.shares = Share.objects.filter(comment=comment).count()
		if request.user.is_authenticated():
			comment.is_liked = Like.objects.filter(comment=comment, user=request.user)
			comment.is_shared = Share.objects.filter(comment=comment, user=request.user)
	paginator = Paginator(comments, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		comments = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		comments = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		comments = paginator.page(paginator.num_pages)

	# list of advertises
	advertises = get_random_advertises()

	return render(request, "topic.html", {	
											'topic': topic,
											'comments': comments,
											'viewers_member': viewers[0],
											'viewers_guest': viewers[1],
											'advertises': advertises
										})






