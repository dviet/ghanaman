from django import forms
from django.utils.translation import ugettext_lazy as _
from tinymce.widgets import TinyMCE
from topics.models import Topic, Comment

class TopicForm(forms.ModelForm):

	class Meta:
		model = Topic
		fields = ['subject']


class CommentForm(forms.ModelForm):

	class Meta:
		model = Comment
		fields = ['message']
		widgets = {
			'message': TinyMCE(attrs={'rows': 20, 'cols': 40}),
		}