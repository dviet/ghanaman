"""
Django settings for Ghanaman project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'sv@204-2b9f1=lc!13qg5+oty&^lvgb+yx@7kx=9cilnqiw5&e'

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'tinymce',
    'userprofile',
    'topics',
    'index',
    'tracking',
    'advertises',
)

MIDDLEWARE_CLASSES = (
    'tracking.middleware.VisitorTrackingMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    #'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'nairaland.middleware.FilterMiddleware',
    'ghanaman.middlewares.ExInFWidgetMiddleware',
)

ROOT_URLCONF = 'ghanaman.urls'

WSGI_APPLICATION = 'ghanaman.wsgi.application'
# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "index.context_processor.header_informations",
    "index.context_processor.categories",
    "django.core.context_processors.request"
)

# Tinymce settings

TINYMCE_DEFAULT_CONFIG = {
    'theme': "modern",
    'width': '100%',
    'height': 200,
    'autoresize_min_height': 200,
    'autoresize_max_height': 800,
    'plugins': [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
}

TINYMCE_SPELLCHECKER = True
TINYMCE_COMPRESSOR = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

TEMPLATE_DIRS = {
    os.path.join(BASE_DIR, 'templates')
}

LOGIN_REDIRECT_URL = '/'

LOGIN_URL = '/login'

SESSION_COOKIE_AGE = 24 * 60 * 60

AUTH_PROFILE_MODULE = 'userprofile.UserProfile'

EMAIL_ADMIN = 'admin@ghanaman.com'

REGISTER_EXPIRED_DATE = 7 * 24 * 60 * 60

NUMBER_ITEM_PERPAGE = 65

TRACK_PAGEVIEWS = True

TRACK_IGNORE_URLS = ['admin/*', 'login', 'logout', '/']

TRACK_IGNORE_STATUS_CODES = [400, 404, 403, 405, 410, 500]

try:
    from local_settings import *

except ImportError:
    import sys, traceback
    sys.stderr.write("Warning: Can't find the file 'local_settings.py' in the directory containing %r. It appears you've customized things.\nYou'll have to run django-admin.py, passing it your settings module.\n(If the file settings.py does indeed exist, it's causing an ImportError somehow.)\n" % __file__)
    sys.stderr.write("\nFor debugging purposes, the exception was:\n\n")
    traceback.print_exc()