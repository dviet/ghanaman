"""
WSGI config for ghanaman project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os, sys

sys.path.append('/home/ghanaman/public_html')
sys.path.append('/home/ghanaman/public_html/ghanaman')
sys.path.append('/home/ghanaman/public_html/ghanaman/ghanaman')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ghanaman.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()