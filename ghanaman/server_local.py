# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['*']

STATIC_URL = '/static/'
STATIC_ROOT = ''

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
TINYMCE_JS_URL = os.path.join(STATIC_URL, "tinymce/tinymce.min.js")
TINYMCE_JS_ROOT = os.path.join(STATIC_ROOT, "tinymce/")

# send mail settings
NEW_USER_EMAIL_SUBJECT = "A new user has been added"
ADMINS = (('Admin', 'support@ghanaman.com'), )
EMAIL_SUBJECT_PREFIX = "[Ghanaman.com] "
EMAIL_HOST = 'ns1.ghanaman.com'
EMAIL_HOST_USER = 'support@ghanaman.com'
EMAIL_HOST_PASSWORD = 'vttrader'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'