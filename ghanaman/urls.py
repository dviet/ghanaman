from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
# from django.contrib.auth.views import login
from index.utils import anonymous_required
from django.conf import settings
from index.views import login_custom

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ghanaman.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^', include('userprofile.urls')),
    url(r'^', include('topics.urls')),
    url(r'^', include('index.urls')),
    url(r'^advertise/', include('advertises.urls')),
    url(r'^tracking/', include('tracking.urls')),
    # url(r'^login/$', anonymous_required(login), {'template_name': 'login.html'}),
    url(r'^login/$', login_custom),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
)


if settings.DEBUG:
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$','django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
	)