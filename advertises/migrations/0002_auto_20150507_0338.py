# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import advertises.models


class Migration(migrations.Migration):

    dependencies = [
        ('advertises', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='advertise',
            name='added_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='advertise',
            name='expired_date',
            field=models.DateTimeField(default=advertises.models.expired_date),
            preserve_default=True,
        ),
    ]
