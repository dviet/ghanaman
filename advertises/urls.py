from django.conf.urls import patterns, url

from advertises.views import home

urlpatterns = patterns('',
	url(r'^home/(?P<advertise_id>\d+)/$', home, name='advertise_home'),
)