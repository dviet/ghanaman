from django.db import models
from django.utils import timezone

# Create your models here.
def expired_date():
    return timezone.now() + timezone.timedelta(days=7)

class Advertise(models.Model):
	image = models.ImageField(upload_to = 'advertises')
	link = models.URLField(max_length=255)
	priority = models.BooleanField(default=False)
	added_date = models.DateTimeField(default=timezone.now)
	expired_date = models.DateTimeField(default=expired_date)

	def __unicode__(self):  
		return self.link