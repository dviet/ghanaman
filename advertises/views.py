import datetime
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from advertises.models import Advertise

# Create your views here.
def home(request, advertise_id):
	advertise = Advertise.objects.get(pk=advertise_id)
	url = advertise.link

	return HttpResponseRedirect(url)

def get_random_advertises():
	from random import sample, shuffle
	advertises_list = []
	time_now = datetime.date.today()
	count = Advertise.objects.filter(expired_date__gte=time_now).count()
	if count > 0:
		advertises = Advertise.objects.filter(expired_date__gte=time_now).order_by('?')

		# get priority item 
		advertise = Advertise.objects.filter(priority=True, expired_date__gte=time_now)
		
		for ad in advertises:
			advertises_list.append(ad)
			if len(advertises_list) == 5:
				break
		if advertise:
			advertises_list.append(advertise[0])
	shuffle(advertises_list)

	return advertises_list

