# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('topics', '0002_auto_20150423_0227'),
        ('userprofile', '0013_auto_20150507_0907'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='mod_category',
            field=models.ManyToManyField(to='topics.TopicCategory', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='mod_type',
            field=models.CharField(default=0, max_length=1, null=True, verbose_name='Mod Type', choices=[(b'0', b'Normal'), (b'1', b'Super Mod'), (b'2', b'Mode')]),
        ),
    ]
