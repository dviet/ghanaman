# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('topics', '0002_auto_20150423_0227'),
        ('userprofile', '0006_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='likes',
            field=models.ManyToManyField(related_name=b'profile_likes', null=True, to='topics.Topic', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='shares',
            field=models.ManyToManyField(related_name=b'profile_shares', null=True, to='topics.Topic', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='emailmessage',
            name='is_read',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='emailmessage',
            name='send_again',
            field=models.BigIntegerField(default=None),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='followed_boards',
            field=models.ManyToManyField(related_name=b'profile_followed_boards', null=True, to=b'topics.TopicCategory', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='followed_topics',
            field=models.ManyToManyField(related_name=b'profile_followed_topics', null=True, to=b'topics.Topic', blank=True),
        ),
    ]
