# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0012_userprofile_mod_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='mod_type',
            field=models.CharField(default=0, max_length=1, null=True, verbose_name='Moderator Type', choices=[(b'1', b'Super Mod'), (b'2', b'Mode')]),
        ),
    ]
