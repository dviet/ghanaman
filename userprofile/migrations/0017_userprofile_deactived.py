# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0016_userprofile_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='deactived',
            field=models.BooleanField(default=0),
            preserve_default=True,
        ),
    ]
