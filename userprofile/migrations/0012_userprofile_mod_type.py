# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0011_auto_20150428_0721'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='mod_type',
            field=models.CharField(default=None, max_length=1, null=True, verbose_name='Moderator Type', choices=[(b'1', b'Super Mod'), (b'2', b'Mode')]),
            preserve_default=True,
        ),
    ]
