# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0007_auto_20150428_0217'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='likes',
            field=models.ManyToManyField(related_name=b'profile_likes', null=True, to=b'topics.Comment', blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='shares',
            field=models.ManyToManyField(related_name=b'profile_shares', null=True, to=b'topics.Comment', blank=True),
        ),
    ]
