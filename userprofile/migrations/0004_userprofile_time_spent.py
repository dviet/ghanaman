# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userprofile', '0003_auto_20150423_0247'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='time_spent',
            field=models.BigIntegerField(null=True),
            preserve_default=True,
        ),
    ]
