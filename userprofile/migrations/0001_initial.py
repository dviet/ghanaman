# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_of_birth', models.DateField(null=True, verbose_name='Date of birth', blank=True)),
                ('gender', models.CharField(default=None, max_length=1, null=True, verbose_name='Gender', choices=[(b'M', b'Male'), (b'F', b'Female')])),
                ('personal_text', models.CharField(max_length=255, null=True, blank=True)),
                ('signature', models.CharField(max_length=255, null=True, blank=True)),
                ('avatar', models.ImageField(null=True, upload_to=b'avatar', blank=True)),
                ('website_title', models.CharField(max_length=255, null=True, blank=True)),
                ('website_url', models.URLField(max_length=255, null=True, blank=True)),
                ('location', models.CharField(max_length=255, null=True, blank=True)),
                ('yim', models.CharField(max_length=255, null=True, blank=True)),
                ('twitter', models.CharField(max_length=255, null=True, blank=True)),
                ('user', models.OneToOneField(related_name=b'profile', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
