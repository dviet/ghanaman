# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('topics', '0002_auto_20150423_0227'),
        ('userprofile', '0004_userprofile_time_spent'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='followed_boards',
            field=models.ManyToManyField(to='topics.TopicCategory', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='followed_topics',
            field=models.ManyToManyField(to='topics.Topic', null=True, blank=True),
            preserve_default=True,
        ),
    ]
