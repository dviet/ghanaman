from django.contrib import admin

# Register your models here.
from userprofile.models import UserProfile, EmailMessage, Follow, Like, Share

admin.site.register(UserProfile)
admin.site.register(EmailMessage)
admin.site.register(Follow)
admin.site.register(Like)
admin.site.register(Share)