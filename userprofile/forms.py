from django import forms
from django.forms import ModelForm, extras
from django.contrib.auth.models import User
from userprofile.models import UserProfile
from topics.models import Topic, TopicCategory



class EditProfileForm(ModelForm):
	date_of_birth = forms.DateField(widget=extras.SelectDateWidget(years=range(1950, 2012)))
	class Meta:
		model = UserProfile
		exclude = ['user', 'mod_type', 'mod_category']


class ChangeEmailForm(ModelForm):
	class Meta:
		model = User
		fields = ['email']


	def clean_email(self):
		new_email = self.cleaned_data['email']
		if User.objects.filter(email=new_email).exists() and not self.instance.email == new_email:
			raise forms.ValidationError("This email already used")
		return new_email


class FollowBoardForm(ModelForm):
	class Meta:
		model = UserProfile
		fields = ['followed_boards']
		widgets = {
			'followed_boards': forms.Select(),
		}

	def __init__(self, *args, **kwargs):
		super(FollowBoardForm, self).__init__(*args, **kwargs)
		profile = UserProfile.objects.get(pk=self.instance.pk)
		if 'followed_boards' in self.initial:
			self.fields['followed_boards'].queryset = TopicCategory.objects.all().exclude(pk__in=profile.followed_boards.all())

class SendMailToSuperMod(forms.Form):
	subject = forms.CharField(max_length=500)
	message = forms.CharField(widget=forms.Textarea)
	
	




