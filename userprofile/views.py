import json
import time
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.conf import settings
from userprofile.forms import EditProfileForm, ChangeEmailForm, FollowBoardForm, SendMailToSuperMod
from userprofile.models import UserProfile, EmailMessage, Follow, Like, Share
from index.models import Register
from tracking.models import Visitor
from topics.models import Topic, TopicCategory, Comment
from advertises.views import get_random_advertises

from index.utils import generate_token
from index.views import page_not_found

# Create your views here.
@login_required
def profile_edit(request):
	# if this is a POST request we need to process the form data
	profile = UserProfile.objects.get(user_id=request.user.id)
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = EditProfileForm(request.POST, request.FILES, instance=profile)
		# check whether it's valid:
		if form.is_valid():
			# save information
			form.save()
			return HttpResponseRedirect('/profile/' + str(request.user.username))

	# if a GET (or any other method) we'll create a blank form
	else:
		form = EditProfileForm(instance=profile)

	return render(request, 'profile_edit.html', {'form': form})

@login_required
def profile(request, username):
	user =  User.objects.get(username=username)

	profile = UserProfile.objects.get(user_id=user.id)
	visitors = Visitor.objects.filter(user_id=user.id)

	time_spent = 0

	for visitor in visitors:
		time_spent = time_spent + visitor.time_on_site

	if Follow.objects.filter(follower=request.user.profile, following=profile):
		is_followed = True
	else:
		is_followed = False

	if Follow.objects.filter(follower=profile, following=request.user.profile):
		is_follow = True
	else:
		is_follow = False

	return render(request, 'profile.html', {
			'Currentuser' : user, 
			'profile' : profile, 
			'time_spent' : time.strftime('%H:%M:%S', time.gmtime(float(time_spent))),
			'is_followed': is_followed,
			'is_follow': is_follow,
		})


@login_required
def email_change(request):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = ChangeEmailForm(request.POST, instance=request.user)
		# check whether it's valid:
		if form.is_valid():
			# update register again
			user =  User.objects.get(id=request.user.id)
			register = Register.objects.get(email=user.email)
			register.email = form.cleaned_data['email']
			register.save()

			# save information
			form.save()
			return HttpResponseRedirect('/profile_edit')

	# if a GET (or any other method) we'll create a blank form
	else:
		form = ChangeEmailForm(instance=request.user)

	return render(request, 'email_change.html', {'form': form})

@login_required
def follow_board(request, board_id):
	try:
		board = TopicCategory.objects.get(pk=board_id)
	except TopicCategory.DoesNotExist:
		return page_not_found(request)

	if request.method == 'POST':
		user = User.objects.get(pk=request.user.id)
		profile = UserProfile.objects.get(user=user)
		if request.POST.get('follow') == 'true':
			profile.followed_boards.add(board)
			follow = True
		else:
			profile.followed_boards.remove(board)
			follow = False

		return HttpResponse(json.dumps({"success": True, "follow": follow}))
	return HttpResponse(json.dumps({"success": False}))

@login_required
def followed_boards(request):
	user = User.objects.get(pk=request.user.id)
	profile = UserProfile.objects.get(user=user)

	sort = request.GET.get('sort')

	if request.method == 'POST':
		board_id = request.POST.get('followed_boards')
		board = TopicCategory.objects.get(pk=int(board_id))
		profile.followed_boards.add(board)
		
		return HttpResponseRedirect('/followed_boards')
	else:
		form = FollowBoardForm(instance=profile)
	if sort == 'create_time':
		topics_list = Topic.objects.filter(category__in=profile.followed_boards.all()).order_by('-date')
	elif sort == 'update_time':
		topics_list = Topic.objects.filter(category__in=profile.followed_boards.all()).order_by('-updated_at')
	else:
		topics_list = Topic.objects.filter(category__in=profile.followed_boards.all()).order_by('-date')
		

	for topic in topics_list:
		topic.last_comment = topic.comment_topic.all().reverse()[0]
	paginator = Paginator(topics_list, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		topics = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		topics = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		topics = paginator.page(paginator.num_pages)

	# list of advertises
	advertises = get_random_advertises()

	return render(request, "topics.html", {	
											'title': 'Followed Boards',
											'topics': topics,
											'follow': True,
											'form': form,
											'profile': profile,
											'advertises': advertises,
											})

@login_required
def follow_topic(request, topic_id):
	try:
		topic = Topic.objects.get(pk=topic_id)
	except Topic.DoesNotExist:
		return page_not_found(request)

	if request.method == 'POST':
		user = User.objects.get(pk=request.user.id)
		profile = UserProfile.objects.get(user=user)
		if request.POST.get('follow') == 'true':
			profile.followed_topics.add(topic)
			follow = True
		else:
			profile.followed_topics.remove(topic)
			follow = False

		return HttpResponse(json.dumps({"success": True, "follow": follow}))
	return HttpResponse(json.dumps({"success": False}))

@login_required
def followed_topics(request):
	user = User.objects.get(pk=request.user.id)
	profile = UserProfile.objects.get(user=user)

	topics_list = profile.followed_topics.all().order_by('-date')
	for topic in topics_list:
		topic.last_comment = topic.comment_topic.all().reverse()[0]
	paginator = Paginator(topics_list, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		topics = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		topics = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		topics = paginator.page(paginator.num_pages)

	# list of advertises
	advertises = get_random_advertises()

	return render(request, "topics.html", {	
											'title': 'Followed Topics',
											'topics': topics,
											'profile': profile,
											'follow_topic': True,
											'advertises': advertises,
											})
@login_required
def send_email_message(request, username):
	user =  User.objects.get(username=username)
	currentUser = User.objects.get(id=request.user.id)
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		if user.id != request.user.id:

			# send mail
			recipients = user.email
			subject = 'Ghanaman - ' + currentUser.username + ' would like to send you a message'
			
			message = currentUser.username + " would like to send you an email message, but since you're not connected, you'll need to contact " + currentUser.username + " by replying this email.\n\n\n Regards, \n Ghanaman Team, \n For: http://www.ghanaman.com/" + currentUser.username
			
			try:
				# send confirmation email
				send_mail(subject, message, currentUser.email, [recipients])
				# save register information
				reg = EmailMessage(sender=currentUser.id, receiver=user.id)
				reg.save()

			except Exception as e:
				print e

			return render(request, 'sent.html', {'receiver' : user.username})
		else:
			return render(request, 'cannotsend.html')

	return render(request, 'sendmail.html', {'sender' : currentUser.username, 'receiver' : user.username})

@login_required
def likes(request, comment_id):
	try:
		comment = Comment.objects.get(pk=comment_id)
	except Comment.DoesNotExist:
		return page_not_found(request)

	if request.method == 'POST':
		user = User.objects.get(pk=request.user.id)
		if request.POST.get('like') == 'true':
			if not Like.objects.filter(user=user, comment=comment):
				like = Like()
				like.user = user
				like.comment = comment
				like.save()
			like = True
		else:
			like = Like.objects.get(user=user, comment=comment)
			like.delete()
			like = False
		likes = Like.objects.filter(comment=comment).count()

		return HttpResponse(json.dumps({"success": True, "like": like, "likes": likes}))
	return HttpResponse(json.dumps({"success": False}))

@login_required
def like_topics(request):
	user = User.objects.get(pk=request.user.id)
	likes = Like.objects.filter(user=user).order_by('-date').values('comment')
	comments = Comment.objects.filter(id__in=likes)
	for comment in comments:
		comment.likes = Like.objects.filter(comment=comment).count()
		comment.is_liked = Like.objects.filter(comment=comment, user=user)
		comment.shares = Share.objects.filter(comment=comment).count()
		comment.is_shared = Share.objects.filter(comment=comment, user=user)

	paginator = Paginator(comments, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		comments = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		comments = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		comments = paginator.page(paginator.num_pages)

	return render(request, "comments.html", {	
											'title': 'My Liked Posts',
											'comments': comments,
											'profile': profile,
											'breadcrum': {'link': 'like_topics', 'title': 'My Liked Posts'}
											})

@login_required
def shares(request, comment_id):
	try:
		comment = Comment.objects.get(pk=comment_id)
	except Comment.DoesNotExist:
		return page_not_found(request)

	if request.method == 'POST':
		user = User.objects.get(pk=request.user.id)
		if request.POST.get('share') == 'true':
			if not Share.objects.filter(user=user, comment=comment):
				share = Share()
				share.user = user
				share.comment = comment
				share.save()
			share = True
		else:
			share = Share.objects.get(user=user, comment=comment)
			share.delete()
			share = False
		shares = Share.objects.filter(comment=comment).count()

		return HttpResponse(json.dumps({"success": True, "share": share, "shares": shares}))
	return HttpResponse(json.dumps({"success": False}))

@login_required
def share_topics(request):
	user = User.objects.get(pk=request.user.id)
	shares = Share.objects.filter(user=user).order_by('-date').values('comment')
	comments = Comment.objects.filter(id__in=shares)
	for comment in comments:
		comment.likes = Like.objects.filter(comment=comment).count()
		comment.is_liked = Like.objects.filter(comment=comment, user=user)
		comment.shares = Share.objects.filter(comment=comment).count()
		comment.is_shared = Share.objects.filter(comment=comment, user=user)
	paginator = Paginator(comments, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		comments = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		comments = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		comments = paginator.page(paginator.num_pages)

	return render(request, "comments.html", {	
											'title': 'My Shared Posts',
											'comments': comments,
											'profile': profile,
											'breadcrum': {'link': 'share_topics', 'title': 'My Shared Posts'}
											})

@login_required
def follow_member(request, profile_id):
	try:
		profile = UserProfile.objects.get(pk=profile_id)
	except UserProfile.DoesNotExist:
		return page_not_found(request)

	if request.method == 'POST':
		user = User.objects.get(pk=request.user.id)
		current_profile = UserProfile.objects.get(user=user)
		if request.POST.get('follow_member') == 'true':
			if not Follow.objects.filter(follower=profile, following=current_profile):
				follow = Follow()
				follow.follower = profile
				follow.following = current_profile
				follow.save()
			follow_member = True
		else:
			follow = Follow.objects.get(follower=profile, following=current_profile)
			follow.delete()
			follow_member = False

		return HttpResponse(json.dumps({"success": True, "follow_member": follow_member}))
	return HttpResponse(json.dumps({"success": False}))

@login_required
def followers(request):
	user = User.objects.get(pk=request.user.id)
	profile = UserProfile.objects.get(user=user)

	followers = Follow.objects.filter(follower=profile)
	for follower in followers:
		follower.follow = Follow.objects.filter(follower=follower.following, following=profile)
	paginator = Paginator(followers, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		followers = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		followers = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		followers = paginator.page(paginator.num_pages)

	return render(request, "followers.html", {	
											'title': 'Followers',
											'followers': followers,
											'profile': profile,
											})

@login_required
def followings(request):
	user = User.objects.get(pk=request.user.id)
	profile = UserProfile.objects.get(user=user)
	followings = Follow.objects.filter(following=profile)
	users = Follow.objects.filter(following=profile).values("follower__user")

	comments = Comment.objects.filter(author__in=users).order_by('-date')
	for comment in comments:
		comment.likes = Like.objects.filter(comment=comment).count()
		comment.is_liked = Like.objects.filter(comment=comment, user=user)
		comment.shares = Share.objects.filter(comment=comment).count()
		comment.is_shared = Share.objects.filter(comment=comment, user=user)

	paginator = Paginator(comments, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		comments = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		comments = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		comments = paginator.page(paginator.num_pages)

	# list of advertises
	advertises = get_random_advertises()

	return render(request, "comments.html", {	
											'title': "Latest Posts By People You're Following",
											'comments': comments,
											'profile': profile,
											'followings': followings,
											'is_following': True,
											'advertises': advertises,
											'breadcrum': {'link': 'followings', 'title': 'Followings'}
											})

@login_required
def likes_and_shares(request):
	user = User.objects.get(pk=request.user.id)
	profile = UserProfile.objects.get(user=user)
	followings = Follow.objects.filter(following=profile)

	comments = Comment.objects.filter(author=user).order_by('-date')
	comments_list = []
	for comment in comments:
		if Like.objects.filter(comment=comment) or Share.objects.filter(comment=comment):
			comment.likes = Like.objects.filter(comment=comment).count()
			comment.is_liked = Like.objects.filter(comment=comment, user=user)
			comment.shares = Share.objects.filter(comment=comment).count()
			comment.is_shared = Share.objects.filter(comment=comment, user=user)
			comments_list.append(comment)

	paginator = Paginator(comments_list, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		comments = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		comments = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		comments = paginator.page(paginator.num_pages)

	return render(request, "comments.html", {	
											'title': "My Likes & Shares",
											'comments': comments,
											'profile': profile,
											'followings': followings,
											'is_following': True,
											'is_liked_and_shared': True,
											'breadcrum': {'link': 'likes_and_shares', 'title': 'My Likes & Shares'}
											})

@login_required
def shared(request):
	user = User.objects.get(pk=request.user.id)
	profile = UserProfile.objects.get(user=user)
	followings = Follow.objects.filter(following=profile).values('follower__user')
	shares = Share.objects.filter(user__in=followings).order_by('date')

	comments = Comment.objects.filter(id__in=shares.values('comment'))
	comments_list = []
	for comment in comments:
		comment.likes = Like.objects.filter(comment=comment).count()
		comment.is_liked = Like.objects.filter(comment=comment, user=user)
		comment.shares = Share.objects.filter(comment=comment).count()
		comment.is_shared = Share.objects.filter(comment=comment, user=user)

		for share in shares:
			if share.comment == comment:
				comment.share = share
				break

	paginator = Paginator(comments, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		comments = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		comments = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		comments = paginator.page(paginator.num_pages)

	return render(request, "comments.html", {	
											'title': "Posts Shared With Me",
											'comments': comments,
											'profile': profile,
											'is_shared_posts': True,
											'breadcrum': {'link': 'shared', 'title': 'Posts Shared With Me'}
											})


@login_required
def send_confirmation_email_for_account_deactivation(request):
	return render(request, 'send_confirmation_email_for_account_deactivation.html')

@login_required
def do_confirmation_email_for_account_deactivation(request):
	try:
		user_profile = UserProfile.objects.get(user=request.user)
		recipients = request.user.email
		subject = 'Ghanaman - Email Confirmation Account Deactivation'
		# will be used when confirm resgistration
		code = generate_token()
		link = 'http://' + request.META['HTTP_HOST'] + '/deactivate_account/' + code

		message = "Dear " + request.user.username + "\nClick on " + link + " to continue with your Nairaland account deactivation process."
		# send confirmation email
		send_mail(subject, message, settings.EMAIL_ADMIN, [recipients])
		# save code of confirmation to db
		user_profile.code = code
		user_profile.save()

	except:
		return page_not_found(request)

	return render(request, 'do_confirmation_email_for_account_deactivation.html')

@login_required 
def deactivate_account(request, code):
	try:
		user_profile = UserProfile.objects.get(user=request.user)
	except Exception, e:
		raise e
	
	if user_profile.code == code:
		if request.method == 'POST' and request.POST.get('confirmed'):
			# chang code field and assign true value to deactived field
			user_profile.code = generate_token()
			user_profile.deactived = 1
			user_profile.save()

			return HttpResponseRedirect('/logout')
	else:
		return render(request, 'invalidcode.html')


	return render(request, 'deactivate_account.html', {'code': code})

@login_required
def send_mail_to_supermod(request, board_id):
	if request.method == 'POST':
		try:
			if int(board_id) == 0:
				mods = UserProfile.objects.filter(mod_type='1')
			else:
				mods = UserProfile.objects.filter(mod_type='1', mod_category__id=board_id)
			
			form = SendMailToSuperMod(request.POST)

			if mods and form.is_valid():
				subject = 'Ghanaman - ' + request.POST.get('subject')
				message = "Dear " + request.user.username + ",\nYou have just received a message with the following content:\n\n" + request.POST.get('message') + '\n\nBest Regards,\nGhanaman Forum Team.'
				for mod in mods:
					# send confirmation email
					send_mail(subject, message, settings.EMAIL_ADMIN, [mod.user.email])

				return HttpResponseRedirect('/')
				
		except Exception, e:
			return page_not_found(request)
	else:
		form = SendMailToSuperMod()

	return render(request, 'send_mail_to_supermod.html', {'form': form, 'board_id': board_id})


