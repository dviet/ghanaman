from django.conf.urls import patterns, url
from userprofile.views import profile, profile_edit, email_change, follow_board, followed_boards, \
	follow_topic, followed_topics, likes, like_topics, shares, share_topics, follow_member, \
	followers, followings, likes_and_shares, shared
from userprofile.views import send_email_message, send_confirmation_email_for_account_deactivation, \
    do_confirmation_email_for_account_deactivation, deactivate_account, send_mail_to_supermod

urlpatterns = patterns('',
    url(r'^email/change/$', email_change, name='email_change'),
    url(r'^profile/([\w\+/. ]+)$', profile, name='profile'),
    url(r'^profile_edit/$', profile_edit, name='profile_edit'),
	url(r'^profile/sendmail/([\w\+/. ]+)/$', send_email_message, name='send_email_message'),
	url(r'^follow_board/(?P<board_id>\d+)/$', follow_board, name='follow_board'),
    url(r'^followed_boards/$', followed_boards, name='followed_boards'),
    url(r'^follow_topic/(?P<topic_id>\d+)/$', follow_topic, name='follow_topic'),
    url(r'^followed_topics/$', followed_topics, name='followed_topics'),
    url(r'^likes/(?P<comment_id>\d+)/$', likes, name='likes'),
    url(r'^like_topics/$', like_topics, name='like_topics'),
    url(r'^shares/(?P<comment_id>\d+)/$', shares, name='shares'),
    url(r'^share_topics/$', share_topics, name='share_topics'),
    url(r'^follow_member/(?P<profile_id>\d+)/$', follow_member, name='follow_member'),
    url(r'^followers/$', followers, name='followers'),
    url(r'^followings/$', followings, name='followings'),
    url(r'^likes_and_shares/$', likes_and_shares, name='likes_and_shares'),
    url(r'^shared/$', shared, name='shared'),
    url(r'^send_confirmation_email_for_account_deactivation/$', send_confirmation_email_for_account_deactivation, name='send_confirmation_email_for_account_deactivation'),
    url(r'^do_confirmation_email_for_account_deactivation/$', do_confirmation_email_for_account_deactivation, name='do_confirmation_email_for_account_deactivation'),
    url(r'^deactivate_account/(?P<code>\w+)$', deactivate_account, name='deactivate_account'),
    url(r'^sendmailtosupermod/(?P<board_id>\d+)$', send_mail_to_supermod, name='send_mail_to_supermod'),
)