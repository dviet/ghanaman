from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils import timezone
from topics.models import Comment

# Create your models here.
		
class UserProfile(models.Model):
	# gender choices
	GENDER_CHOICES = (('M','Male'), ('F','Female'))
	# type moderator choices
	MOD_TYPE = (
			('0', 'Normal'),
			('1', 'Super Mod'), 
			('2', 'Mod')
		)
	# variable use to store year old of user
	age = 0

	user = models.OneToOneField(User, related_name="profile")
	date_of_birth = models.DateField(_('Date of birth'), blank = True, null = True)
	gender = models.CharField(_('Gender'), max_length=1, blank=False, null = True, choices = GENDER_CHOICES, default = None)
	personal_text = models.CharField(max_length=255, null=True, blank=True)
	signature = models.CharField(max_length=255, null=True, blank=True)
	avatar = models.ImageField(upload_to = 'avatar', blank = True, null = True)
	website_title = models.CharField(max_length=255, null=True, blank=True)
	website_url = models.URLField(max_length=255, null=True, blank=True)
	location = models.CharField(max_length=255, null=True, blank=True)
	yim = models.CharField(max_length=255, null=True, blank=True)
	twitter = models.CharField(max_length=255, null=True, blank=True)
	followed_boards = models.ManyToManyField('topics.TopicCategory', null=True, blank=True, related_name="profile_followed_boards")
	followed_topics = models.ManyToManyField('topics.Topic', null=True, blank=True, related_name="profile_followed_topics")
	mod_type = models.CharField(_('Mod Type'), max_length=1, blank=False, null = True, choices = MOD_TYPE, default = 0)
	mod_category = models.ManyToManyField('topics.TopicCategory', null=True, blank=True)
	code = models.CharField(max_length=200, blank=True, null=True)
	deactived = models.BooleanField(blank=True, default=0)

	def __str__(self):  
		return "%s's profile" % self.user

def create_user_profile(sender, instance, created, **kwargs):  
	if created:  
		profile, created = UserProfile.objects.get_or_create(user=instance)  

post_save.connect(create_user_profile, sender=User)

class EmailMessage(models.Model):
	sender = models.IntegerField()
	receiver = models.IntegerField()
	is_read = models.IntegerField(default=0)
	send_again = models.BigIntegerField(default=None)


class Follow(models.Model):
	follower = models.ForeignKey(UserProfile, related_name='profile_follower') # profile is followed
	following = models.ForeignKey(UserProfile, related_name='profile_following') # profile follows
	date = models.DateTimeField(default=timezone.now)


class Like(models.Model):
	user = models.ForeignKey(User, related_name='user_like')
	comment = models.ForeignKey(Comment, related_name='comment_like')
	date = models.DateTimeField(default=timezone.now)


class Share(models.Model):
	user = models.ForeignKey(User, related_name='user_share')
	comment = models.ForeignKey(Comment, related_name='comment_share')
	date = models.DateTimeField(default=timezone.now)
