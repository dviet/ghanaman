import time
from datetime import datetime 
from django.shortcuts import render, render_to_response
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import resolve
from django.utils import timezone
from django.db.models import Q
from django.db.models import Count

from index.models import Register
from index.utils import generate_token, get_query, normalize_query
from index.forms import RegisterForm, ConfirmRegisterForm, SearchForm

from topics.models import Topic, TopicCategory, Comment
from tracking.models import Visitor
from advertises.views import get_random_advertises

from userprofile.models import Like, Share
from userprofile.models import UserProfile



# Create your views here.

def main_page(request):
	topics_list = Topic.objects.all().order_by('-date')
	paginator = Paginator(topics_list, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		topics = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		topics = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		topics = paginator.page(paginator.num_pages)

	categories_parent = TopicCategory.objects.filter(parent=None, is_active=True).order_by('title')
	for category in categories_parent:
		categories_children = TopicCategory.objects.filter(parent=category, is_active=True).order_by('title')
		category.children = categories_children

	# statistical members and guest are online
	visitor_member = Visitor.objects.filter(\
						Q(user_id__isnull=False),\
						Q(expiry_time__gt=timezone.now()), \
						Q(end_time__isnull=True))\
					.annotate(Count('user'))

	visitor_guest = Visitor.objects.filter(\
						Q(user_id__isnull=True),\
						Q(expiry_time__gt=timezone.now()),\
						Q(end_time__isnull=True))\
					.annotate(Count('user'))

	# list of member's birthdays
	date_now = datetime.now()\
				.strftime("%m-%d")
	year_now = datetime.now()\
				.strftime("%Y")

	members_birthday = UserProfile.objects.filter(date_of_birth__contains=date_now)

	for member_birthday in members_birthday:
		if member_birthday.date_of_birth: 
			member_birthday.age = calculate_age(year_now, member_birthday.date_of_birth)

	# list of advertises
	advertises = get_random_advertises()

	return render(request, "index.html", {	
											'topics': topics,
											'categories_parent': categories_parent,
											'visitor_member' : len(visitor_member),
											'visitor_guest' : len(visitor_guest),
											'members_birthday' : members_birthday,
											'pagination' : pagination(topics),
											'advertises': advertises
										})



def pagination(obj):
	pagi = []
	next_to = 2
	total = obj.paginator.num_pages
	average = int(obj.paginator.num_pages / 2)
	cur_page = obj.number

	if total < 10:
		pagi = range(1, total + 1)

	else:
		if cur_page > average:
			if (cur_page + next_to) >= total:
				pagi = [1, '...'] + range(cur_page - next_to, total + 1)
			else:
				pagi = [1, '...'] + range(cur_page - next_to, cur_page + next_to + 1) + [ '...', total]
		else:

			if cur_page - next_to <= 2:
				pagi = range(1, cur_page + next_to) + ['...', total]
			else:
				pagi = [1, '...'] + range(cur_page - next_to, cur_page + next_to + 1) + ['...', total]

	return pagi_template(pagi, cur_page)



def pagi_template(list_pagi, cur_page):

	template = ''
	for item in list_pagi:
		if cur_page == item:
			template += '<span class="current"><b>(' + str(item) + ')</b></span>'
		elif item == '...':
			template += '<span>(' + str(item) + ')</span>'
		else:
			template += '<a href="?page=' + str(item) + '">(' + str(item) + ')</a>'

	return template


def calculate_age(year_now, birthday):
	return int(year_now) - int(birthday.year)

def page_not_found(request):
    return render(request, "404.html", status=404)


def login_custom(request):
	# logout if user logged in
	logout(request)
	message = ''
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']

		user = authenticate(username=username, password=password)
		if user is not None:
			try:
				userprofile = UserProfile.objects.get(user=user)
				if userprofile and userprofile.deactived:
					return render(request, 'deactived_account.html')
			except Exception, e:
				pass

			if user.is_active:
				login(request, user)
				return HttpResponseRedirect('/')
		else:
			message = 'Username or password is not correct.'
	return render(request, 'login.html', {'message': message})
	

def register(request):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = RegisterForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			# send mail
			recipients = form.cleaned_data['email']
			subject = 'Ghanaman - Email Confirmation'
			# will be used when confirm resgistration
			code = generate_token()
			link = 'http://' + request.META['HTTP_HOST'] + '/register/confirm/' + recipients + '/' + code

			message = "Please click here to complete your registration: " + link + " (You must complete your registration within a few days)"
			
			try:
				# send confirmation email
				send_mail(subject, message, settings.EMAIL_ADMIN, [recipients])
				# save register information
				expired_date = settings.REGISTER_EXPIRED_DATE + int(time.time())
				reg = Register(email=recipients, code=code, expired_date=expired_date)
				reg.save()

			except Exception as e:
				print e

			return HttpResponseRedirect('/register/thanks/');

	# if a GET (or any other method) we'll create a blank form
	else:
		form = RegisterForm()

	return render(request, 'register.html', {'form': form})


def thanks_register(request):
	return render(request, 'thanks_register.html', {});

def confirm_register(request, email='', code=''):

	# check valid code
	reg = Register.objects.get(email=email)
	if not code == reg.code:
		return render(request, 'errors_register_confirm.html', {'message': 'Invalid Code'})

	# check expired link
	time_now = int(time.time())
	
	if time_now > reg.expired_date:
		user = User.objects.filter(email=email)
		if user:
			return render(request, 'errors_register_confirm.html', {'message': 'You were a system\'s user'})
		else:
			reg.delete()
			return render(request, 'errors_register_confirm.html', {'message': 'Registration link is expired. Register again.'})

	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request
		form = ConfirmRegisterForm(request.POST)

		# check whether it's valid
		if form.is_valid():
			# save user's information
			data = form.cleaned_data
			User.objects.create_user(data['username'], data['email'], data['password'])

			return HttpResponseRedirect('/login/');
	else: 
		form = ConfirmRegisterForm(initial={'email': email})

	return render(request, 'confirm_register.html', {'form': form});

def search(request):
	current_url = resolve(request.path_info).url_name
	# set title
	title = 'Search'
	if current_url == 'metions':
		title = 'Metions'

	is_search_page = True
	form_search = SearchForm(request.GET)
	comments = []
	q = request.GET.get('q', '')
	

	category = request.GET.get('category', None)
	if q:
		query = get_query(q, ['message'], 1)
		comments = Comment.objects.filter(query)
		if category:
			comments = comments.filter(topic__category=category)

	for comment in comments:
		comment.likes = Like.objects.filter(comment=comment).count()
		comment.shares = Share.objects.filter(comment=comment).count()
		if request.user.is_authenticated():
			comment.is_liked = Like.objects.filter(comment=comment, user=request.user)
			comment.is_shared = Share.objects.filter(comment=comment, user=request.user)

	paginator = Paginator(comments, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		comments = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		comments = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		comments = paginator.page(paginator.num_pages)

	return render(request, "search.html", {	
											'is_search_page': is_search_page,
											'form_search': form_search,
											'comments': comments,
											'q': q,
											'category': category,
											'title': title
										})

def newtopics(request):
	topics_list = Topic.objects.all().order_by('-date')
	for topic in topics_list:
		topic.last_comment = topic.comment_topic.all().reverse()[0]
	paginator = Paginator(topics_list, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		topics = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		topics = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		topics = paginator.page(paginator.num_pages)

	# list of advertises
	advertises = get_random_advertises()

	return render(request, "topics.html", {	
											'title': 'New Topics',
											'topics': topics,
											'advertises': advertises,
											})

def recent(request):
	today = datetime.today()
	comments = Comment.objects.filter(date__year=today.year, date__month=today.month, date__day=today.day).order_by('-date')
	for comment in comments:
		comment.likes = Like.objects.filter(comment=comment).count()
		comment.shares = Share.objects.filter(comment=comment).count()
		if request.user.is_authenticated():
			comment.is_liked = Like.objects.filter(comment=comment, user=request.user)
			comment.is_shared = Share.objects.filter(comment=comment, user=request.user)

	paginator = Paginator(comments, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		comments = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		comments = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		comments = paginator.page(paginator.num_pages)

	# list of advertises
	advertises = get_random_advertises()

	return render(request, "comments.html", {	
											'title': 'Recent Posts',
											'comments': comments,
											'advertises': advertises,
											'breadcrum': {'link': 'recent', 'title': 'Recent Post'}
											})

def trending(request):
	topics_list = Topic.objects.all().order_by('-date')
	for topic in topics_list:
		topic.last_comment = topic.comment_topic.all().reverse()[0]
	paginator = Paginator(topics_list, settings.NUMBER_ITEM_PERPAGE)

	page = request.GET.get('page')
	try:
		topics = paginator.page(page)
	except PageNotAnInteger:
		# If page is not an integer, deliver first page.
		topics = paginator.page(1)
	except EmptyPage:
		# If page is out of range (e.g. 9999), deliver last page of results.
		topics = paginator.page(paginator.num_pages)

	# list of advertises
	advertises = get_random_advertises()

	return render(request, "topics.html", {	
											'title': 'Trending Topics',
											'topics': topics,
											'advertises': advertises,
											})


def login_without_password(request):

	if request.method == 'POST':
		username_or_email = request.POST.get('username_or_email')
		try:
			user = User.objects.get(Q(username=username_or_email) | Q(email=username_or_email))
			# send code to email
			# send mail
			recipients = user.email
			# will be used when confirm logging
			code = generate_token()
			subject = 'Email Confirmation Code: ' + code
			message = "Dear " + user.username + ",\n\nThis is your latest email confirmation code:\n   " + code + "\nYou can use it only once."
			
			try:
				# send confirmation email
				send_mail(subject, message, settings.EMAIL_ADMIN, [recipients])
				# save register information
				user.profile.code = code
				user.profile.save()
				return HttpResponseRedirect('/entry_code_login')

			except:
				return page_not_found(request)

		except Exception, e:
			return render(request, 'login.html', {'message_user_not_found': 'User or Email doesn\'t not exist'})


def entry_code_login(request):
	if request.method == 'POST':
		pass
	return render(request, 'entry_code_login.html')
		
			







