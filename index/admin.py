from django.contrib import admin

# Register your models here.
from index.models import Register


class RegisterAdmin(admin.ModelAdmin):

	list_display = ('email', 'code', 'expired_date')  
admin.site.register(Register, RegisterAdmin)

