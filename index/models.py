from django.db import models
from tinymce.models import HTMLField

# Create your models here.
class Register(models.Model):
	email = models.CharField(max_length=100, unique=True)
	code = models.CharField(max_length=100, default=None)
	expired_date = models.BigIntegerField(default=None)
