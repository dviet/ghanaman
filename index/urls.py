from django.conf.urls import patterns, url

from index.views import main_page, register, thanks_register, confirm_register, search, newtopics,\
						recent, trending, login_without_password, entry_code_login
from index.utils import anonymous_required

urlpatterns = patterns('',
 	url(r'^$', main_page, name='main_page'),
    url(r'^register/$', anonymous_required(register), name='register'),
    url(r'^register/thanks/$', thanks_register, name='register_thanks'),
    url(r'^register/confirm/([\w\+@/. ]+)/(\w+)$', anonymous_required(confirm_register), name='register_confirm'),
	url(r'^search/$', search, name='search'),
	url(r'^metions/$', search, name='metions'),
	url(r'^newtopics/$', newtopics, name='newtopics'),
	url(r'^recent/$', recent, name='recent'),
	url(r'^trending/$', trending, name='trending'),
	url(r'^login_without_password/$', login_without_password, name='login_without_password'),
	url(r'^entry_code_login/$', entry_code_login, name='entry_code_login'),
)