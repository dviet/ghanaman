from datetime import datetime
from django.contrib.auth.models import User
from topics.models import Topic
from topics.models import TopicCategory
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings

def header_informations(request):
	total_members = User.objects.all().count()
	total_topics = Topic.objects.all().count()

	return {
			'total_members': total_members,
			'total_topics': total_topics
			}


def categories(request):
	categories = TopicCategory.objects.filter(parent__isnull=False)
	featured_links = Topic.objects.filter(is_deleted=False, is_featured=True)
	paginator = Paginator(featured_links, settings.NUMBER_ITEM_PERPAGE)
	if paginator.num_pages >= 9:
		paginator.num_pages = 9

	return {
		'categories': categories,
		'featured_links_range': range(1, paginator.num_pages + 1)
	}

