# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Register',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.CharField(unique=True, max_length=100)),
                ('code', models.CharField(default=None, max_length=100)),
                ('expired_date', models.BigIntegerField(default=None)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
