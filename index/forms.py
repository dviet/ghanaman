from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from index.models import Register
from topics.models import TopicCategory

class RegisterForm(ModelForm):
	class Meta:
		model = Register
		fields = ['email']


class ConfirmRegisterForm(ModelForm):
	password_confirm = forms.CharField(max_length=100, widget=forms.PasswordInput)

	class Meta:
		model = User
		fields = ['email', 'username', 'password']
		widgets = {
            'password': forms.PasswordInput(),
        }

	def clean_password_confirm(self):
		password = self.cleaned_data['password']
		password_confirm = self.cleaned_data['password_confirm']
		
		if not password == password_confirm:
			raise forms.ValidationError("Password mismatch.")
		
		return password_confirm


class SearchForm(forms.Form):
	q = forms.CharField(max_length=255, required=False)
	category = forms.ModelChoiceField(required=False, queryset=TopicCategory.objects.all(), empty_label='-- All section --')
	topiconly = forms.BooleanField(label='Topic')
	imageonly = forms.BooleanField(label='Image')
	

