import re
from django import template
from django.utils.safestring import mark_safe
from django.utils.html import escape
from bs4 import BeautifulSoup


register = template.Library()

@register.filter(name='addclassandplaceholder')
def addclassandplaceholder(field, css):
   return field.as_widget(attrs={"class":css, "placeholder": field.label})

@register.filter(name='addclass')
def addclass(field, css):
   return field.as_widget(attrs={"class": css})

@register.filter(name='highlight')
def highlight(text, filter):
	soup = BeautifulSoup(text)
	pattern = re.compile(r"(?P<filter>%s)" % filter, re.IGNORECASE)
	t = soup.get_text()
	t = re.sub(pattern, r"<span class='highlight'>\g<filter></span>", t)
	return mark_safe(t)